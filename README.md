## 9 by 9 Sudoku board solver

![A board of Sudoku](https://beyondadversity.com/wp-content/uploads/2012/12/Sudoku-1-300x300.png "A board of Sudoku")


Implement a solver for 9 by 9 Sudoku boards. See:

https://en.wikipedia.org/wiki/Sudoku

You will be given an unsolved/incomplete Sudoku board. 

Unsolved fields are marked with '0'.

You will have to find a solution (answer) for every empty field.

Assume that all of the tests cases are solvable by brute-force.